#
# Copyright 2021 Baodong
#
# All Rights Reserved.
#

name "permutations"
maintainer "Baodong"
homepage "https://icbd.github.io"

# Defaults to C:/permutations on Windows
# and /opt/permutations on all other platforms
install_dir "#{default_root}/#{name}"

build_version Omnibus::BuildVersion.semver
build_iteration 1

# Creates required build directories
dependency "preparation"

# permutations dependencies/components
dependency "curl"
dependency "ruby"
dependency "permutations"

exclude "**/.git"
exclude "**/bundler/git"
