#
# Copyright 2021 Baodong
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# These options are required for all software definitions
name "permutations"

default_version "1.0.1"

version("1.0.0") { source md5: "89ae743020c50f655d416be051af76a4" }
# source url: "https://gist.githubusercontent.com/icbd/6004fc07467f01613fe18e73e7bef08e/raw/a7c66ca66fe92191918ada9b8ac5a958a6f5f804/permutations.rb"

version("1.0.1") { source md5: "a17d44353f900d850e918534bfdd6c6f" }
source url: "https://gist.githubusercontent.com/icbd/6004fc07467f01613fe18e73e7bef08e/raw/5c0a4b22c53f03fa93d249f39f52498d9fa71f1c/permutations.rb"

license :project_license
skip_transitive_dependency_licensing true

build do
  # Setup a default environment from Omnibus - you should use this Omnibus
  # helper everywhere. It will become the default in the future.
  env = with_standard_compiler_flags(with_embedded_path)

  command "cp ./permutations.rb #{install_dir}/bin/permutations"
end
